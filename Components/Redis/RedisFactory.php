<?php

namespace Core\Components\Redis;

use Core\Components\Container\Container;
use Exception;
use Redis;

/**
 *
 */
class RedisFactory
{
	/**
	 * @return Redis
	 */
    private function createConnection(): Redis
    {
        $config = Container::get('config')['redis'];
	    $connection = new Redis();
	    $connection->connect(
		    $config['host'],
		    $config['port']
	    );
	    return $connection;
    }

	/**
	 * @return Redis
	 * @throws Exception
	 */
    public function isRedisAvailable(): Redis
    {
	    if (!Container::get('config')['redis']['access']){
		    return new Redis();
	    }
    	return $this->createConnection();
    }

}
