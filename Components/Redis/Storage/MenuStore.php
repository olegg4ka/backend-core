<?php

namespace Core\Components\Redis\Storage;

use Core\Components\Redis\RedisConnection;
use Core\Utils\RedisDataBase\RedisDatabase;

/**
 *
 */
class MenuStore
{
	/**
	 * @param int $userId
	 * @return array
	 */
	public function getMenuByID(int $userId): array
	{
		$connection = RedisConnection::getConnection(RedisDatabase::SYSTEM);
		$data = $connection->hMGet('menuByID', [$userId]);
		return $data[$userId] !== false ? json_decode((string)$data[$userId], true) : [];
	}

	/**
	 * @param array $menu
	 * @param int $userId
	 */
	public function setMenuByID(array $menu, int $userId)
	{
		$connection = RedisConnection::getConnection(RedisDatabase::SYSTEM);
		$connection->hMSet('menuByID', [$userId => json_encode($menu)]);
	}
}
