<?php

namespace Core\Components\Http;

use Core\Components\Container\Container;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteMananger
 * @package Core\Component\Http
 */
class RouteMananger
{
	/**
	 * @var RouteCollection
	 */
    private $routeCollection;
	/**
	 * @var
	 */
    private $matcher;
	/**
	 * @var RequestContext
	 */
    private $context;

	/**
	 * RouteMananger constructor.
	 */
    public function __construct()
    {
        $this->routeCollection = new RouteCollection();
        $this->context = new RequestContext();
    }

	/**
	 *
	 */
    public function handle(): void
    {
        foreach (Container::get('config')['routes'] as $name => $route) {
            $this->routeCollection->add($name, $route);
        }
        $this->matcher = new UrlMatcher($this->routeCollection, $this->context->fromRequest((new Request)->getRequest()));
    }

	/**
	 * @return RequestContext
	 */
    public function getContext(): RequestContext
    {
        return $this->context;
    }

	/**
	 * @return mixed
	 */
    public function getMatcher()
    {
        return $this->matcher;
    }
}
