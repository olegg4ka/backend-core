<?php

namespace Core\Components\Http;


use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResponseAggregator
 * @package Core\Components\Http
 */
class ResponseAggregator
{

	/**
	 * @var Response
	 */
    protected $response;

	/**
	 * ResponseAggregator constructor.
	 * @param $content
	 * @param $status
	 * @param $contentType
	 */
    public function __construct($content = '', int $status = 200, array $contentType = [])
    {
	    $this->response = new Response($content,$status, $contentType);
    }

	/**
	 * @return Response
	 */
    public function getResponse(): Response
    {
        return $this->response;
    }

}