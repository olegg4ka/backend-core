<?php

namespace Core\Components\Database;

use Core\Components\Container\Container;
use Core\Components\Database\Connection\ConnectionInterface;
use Exception;
use PDO;
use Psr\Log\LoggerInterface;

/**
 *
 */
class Executor
{
	/**
	 * @var ConnectionInterface
	 */
	private $connection;

	/**
	 * ProceduresExecutor constructor.
	 * @param ConnectionInterface $connection
	 */
	public function __construct(ConnectionInterface $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * @param string $query
	 * @param array $values
	 * @param int $fetch
	 * @param string $nameDatabase
	 * @return array
	 * @throws Exception
	 */
	public function execute(string $query, array $values, $fetch = PDO::FETCH_ASSOC, string $nameDatabase = 'dnd')
	{
		/**
		 *  @var LoggerInterface $logger
		 */
		$logger = Container::get('Logger');
		$replaceValue = [];
		foreach (array_values($values) as $value) {
			if ($value === null) {
				$replaceValue[] = $value;
				continue;
			}
			$replaceValue[] = "'" . $value . "'";
		}
		$logger->info(str_replace(array_keys($values), $replaceValue, $query));
		$connection = $this->connection->getConnection($nameDatabase);
		$db = $connection->prepare($query);
		$db->execute($values);
		return $db->fetchAll($fetch);
	}
}
