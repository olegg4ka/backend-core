<?php

namespace Core\Components\Logger;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Interface LoggerFactoryInterface
 * @package Core\Components\Logger
 */
interface LoggerFactoryInterface
{
	/**
	 * LoggerFactoryInterface constructor.
	 * @param ContainerInterface $container
	 * @param LoggerInterface $logger
	 */
	public function __construct(ContainerInterface $container, LoggerInterface $logger);

	/**
	 * @param string $name
	 * @param string $handler
	 * @return mixed
	 */
	public function createLogger(string $name, string $handler):LoggerInterface;

}