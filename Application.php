<?php


namespace Core;


use Core\Components\Container\Container;
use Core\Components\Http\RouteMananger;
use Core\Components\Redis\RedisFactory;
use Exception;
use Redis;
use Symfony\Component\HttpFoundation\Session\Session;

class Application
{
	private static $instances = [];

	private $redis;

	private $routes;

	private $session;

	/**
	 * Application constructor.
	 * @throws Exception
	 */
	protected function __construct(){
		$this->getRedisConnection();
		$this->getActiveRoutes();
		$this->startSession();
	}

	/**
	 *
	 */
	protected function __clone(){}

	/**
	 * @throws Exception
	 */
	public function __wakeup()
	{
		throw new Exception('Error singleton');
	}

	/**
	 * @return Application
	 */
	public static function getInstance(): Application{
		$class = static::class;
		if (!isset(self::$instances[$class])){
			self::$instances[$class] = new static();
		}
		return self::$instances[$class];
	}

	/**
	 * @return Redis
	 * @throws Exception
	 */
	public function getRedisConnection()
	{
		return $this->redis = (new RedisFactory())->isRedisAvailable();
	}

	/**
	 * @return RouteMananger
	 */
	public function getActiveRoutes()
	{
		$routeRunner = new RouteMananger;
		$routeRunner->handle();
		return $this->routes = $routeRunner;
	}

	/**
	 * @param null $storage
	 * @return Session
	 */
	public function startSession($storage = null)
	{
		$this->session = new Session($storage);
		$this->session->start();
		return $this->session;
	}

	public function sendRouteToRequest(){
		$routes = Container::get('config')['routes'];
		foreach ($routes as $value){
			if ($this->routes->getContext()->getPathInfo() == $value->getPath()) {
				//todo api controller
			}
		}


		return 0;
	}

	// сравнить что пришло в реквесте. на какой домен и сравнить с роутами и направит ьна короллер

}