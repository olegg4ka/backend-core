<?php

namespace Core\Utils;


/**
 * Class ApplicationState
 */
class ApplicationState
{
	const APP_ON_DESTROY = 'app.destroy';
}