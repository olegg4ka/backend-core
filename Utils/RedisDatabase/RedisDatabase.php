<?php

namespace Core\Utils\RedisDataBase;


/**
 *
 */
class RedisDatabase
{
	const PERMISSION = 15;
	const SESSION_INFORMATION = 14;
	const APPLICATION = 13;
	const SYSTEM = 12;
}