<?php

namespace Core\Utils\Checker;

/**
 * Перевірка айпі
 */
class CheckIpAddress
{
	/**
	 * Метод для отримання IP
	 * $ip - змінна, котра вертає значення IP
	 * @return string
	 */
	public static function checkIpAddress(): string
	{
		foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
			if (array_key_exists($key, $_SERVER) === true) {
				foreach (explode(',', $_SERVER[$key]) as $ip) {
					$ip = trim($ip);
					if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
						return $ip;
					}
					return $ip;
				}
			}
		}
		return '';
	}
}