<?php

namespace Core\Controller\Resources;


/**
 * Class GitController
 */
class GitController
{
	/**
	 * Обновить DEV
	 */
	public function update()
	{
		$command = 'cd ' . ROOT_PATH;
		$command .= ' && git pull --recurse-submodules';
		$command .= ' && git pull --recurse-submodules';
		$command .= ' && git submodule status';
		$res = [];
		exec($command, $res);
		echo "updated\n";
	}
}