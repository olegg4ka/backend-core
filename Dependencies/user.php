<?php


use Core\Components\Auth\User\User;
use Core\Components\Auth\User\UserInterface;
use Psr\Container\ContainerInterface;

return [
    UserInterface::class => DI\factory(function (ContainerInterface $container) {
        return new User;
    })
];
