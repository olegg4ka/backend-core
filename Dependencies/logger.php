<?php

use Core\Components\Auth\User\UserInterface;
use Core\Components\Logger\LoggerFactory;
use Core\Components\Logger\LoggerFactoryInterface;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return [
    LoggerInterface::class => DI\factory(function (ContainerInterface $container) {
        return new Logger($container->get('config')['logger']['name']);
    }),
    LoggerFactoryInterface::class => DI\factory(function (LoggerFactory $loggerFactory) {
        return $loggerFactory;
    }),
    'Logger' => DI\factory(function (ContainerInterface $container, LoggerFactoryInterface $loggerFactory) {

        $userID = $container->get(UserInterface::class)->getUserID();
        if ($userID) {
            return $loggerFactory
                ->setPath('users/' . date('d.m.Y') . '/' . $userID . '/')
                ->createLogger('db_logs');
        }

        if ($userID === null) {
            return $loggerFactory
	            ->setPath('users/' . date('d.m.Y') . '/' . $userID . '/')
                ->createLogger('db_logs');
        }

        return $loggerFactory
            ->createLogger('db_logs');
    })
];
